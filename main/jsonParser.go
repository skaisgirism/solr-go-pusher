package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

//Pushes the data to solr
func DataPusher(data interface{}){
	//Makes copy of struct to Buffer
	structCopy := new(bytes.Buffer)
	//Encodes it
	json.NewEncoder(structCopy).Encode(data)

	resp, err := http.Post("http://192.168.0.2:8983/solr/salonProCore/update/json/docs?&commit=true",
		"application/json; charset=utf-8", structCopy)
	if err != nil {
		fmt.Println(err)
	}
	//log our response
	b, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", b)
}
