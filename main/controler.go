package main

import (
	_ "bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	StructBuilder("../data/15new.json")
	//ValueChecker("../data/4.json")
	//LoadProductData("data/17.json")
	//LoadCategoryData("data/categories.json")
}

func StructBuilder(fileName string) (error, []ProductB){
	var data []ProductB

	infoFile, err := os.Open(fileName)
	if err != nil {
		//return data, err
		log.Fatal(err)
	}
	defer infoFile.Close()

	byteValue, err := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &data)

	return err, data
}

func ValueChecker(fileName string)([]Product, error){
	var data []Product
	infoFile, err := os.Open(fileName)
	if err != nil {
		//return data, err
		log.Fatal(err)
	}
	defer infoFile.Close()
	fmt.Println(fileName, " Opened")

	byteValue, _ := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &data)


	for i := 0; i <= len(data); i++ {
		fmt.Println("entity id: ", data[i].EntityID, data[i].GroupPrice)
	}

	return data, err

}



//Method loads product data from json file
func LoadProductData(fileName string) ([]Product, error){
	var data []Product
	infoFile, err := os.Open(fileName)
	if err != nil {
		//return data, err
		log.Fatal(err)
	}
	defer infoFile.Close()
	fmt.Println(fileName, " Opened")

	byteValue, _ := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &data)

	//Send the data to solr
	DataPusher(data)

	return data, err
}

func LoadCategoryData(fileName string) ([]Category, error){
	var data []Category
	infoFile, err := os.Open(fileName)
	if err != nil {
		//return data, err
		log.Fatal(err)
	}
	defer infoFile.Close()
	fmt.Println(fileName, " Opened")

	byteValue, _ := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &data)

	//Send the data to solr
	DataPusher(data)

	return data, err
}