package main

type Product struct {
	EntityID                   string      `json:"entity_id"`
	EntityTypeID               string      `json:"entity_type_id"`
	AttributeSetID             string      `json:"attribute_set_id"`
	TypeID                     string      `json:"type_id"`
	Sku                        string      `json:"sku"`
	HasOptions                 string      `json:"has_options"`
	RequiredOptions            string      `json:"required_options"`
	CreatedAt                  string      `json:"created_at"`
	UpdatedAt                  string      `json:"updated_at"`
	Status                     string      `json:"status"`
	IsRecurring                string      `json:"is_recurring,omitempty"`
	Visibility                 string      `json:"visibility"`
	EnableGooglecheckout       string      `json:"enable_googlecheckout"`
	TaxClassID                 string      `json:"tax_class_id"`
	IsImported                 string      `json:"is_imported"`
	Featured                   string      `json:"featured"`
	MageFeaturedProduct        string      `json:"mage_featured_product"`
	Date                       string      `json:"date,omitempty"`
	ProductWeight              interface{} `json:"product_weight,omitempty"`
	Price                      string      `json:"price"`
	SpecialPrice               interface{} `json:"special_price"`
	Cost                       interface{} `json:"cost"`
	Weight                     interface{} `json:"weight"`
	Msrp                       interface{} `json:"msrp"`
	Name                       string      `json:"name"`
	MetaTitle                  interface{} `json:"meta_title"`
	MetaDescription            interface{} `json:"meta_description"`
	Image                      string      `json:"image"`
	SmallImage                 string      `json:"small_image"`
	Thumbnail                  string      `json:"thumbnail"`
	URLKey                     string      `json:"url_key"`
	URLPath                    string      `json:"url_path"`
	CustomDesign               interface{} `json:"custom_design"`
	PageLayout                 interface{} `json:"page_layout"`
	OptionsContainer           string      `json:"options_container"`
	CountryOfManufacture       interface{} `json:"country_of_manufacture"`
	MsrpEnabled                string      `json:"msrp_enabled"`
	MsrpDisplayActualPriceType string      `json:"msrp_display_actual_price_type"`
	GiftMessageAvailable       interface{} `json:"gift_message_available"`
	FoundInRivileAPI           string      `json:"found_in_rivile_api"`
	SpecialFromDate            interface{} `json:"special_from_date"`
	SpecialToDate              interface{} `json:"special_to_date"`
	NewsFromDate               interface{} `json:"news_from_date"`
	NewsToDate                 interface{} `json:"news_to_date"`
	CustomDesignFrom           interface{} `json:"custom_design_from"`
	CustomDesignTo             interface{} `json:"custom_design_to"`
	Description                string      `json:"description"`
	ShortDescription           interface{} `json:"short_description"`
	MetaKeyword                interface{} `json:"meta_keyword"`
	CustomLayoutUpdate         interface{} `json:"custom_layout_update"`
	StockItem                  struct{}    `json:"stock_item"`
	UseSmdColorswatch          string      `json:"use_smd_colorswatch,omitempty"`
	SmdColorswatchProductList  string      `json:"smd_colorswatch_product_list,omitempty"`
	ImageLabel                 string      `json:"image_label,omitempty"`
	SmallImageLabel            string      `json:"small_image_label,omitempty"`
	ThumbnailLabel             string      `json:"thumbnail_label,omitempty"`
	SmdCustomStockStatus       interface{} `json:"smd_custom_stock_status,omitempty"`
	Manufacturer               string      `json:"manufacturer,omitempty"`
	Wights                     interface{} `json:"wights,omitempty"`
	Provider                   string      `json:"provider,omitempty"`
	SoldToProfessionals        string      `json:"sold_to_professionals,omitempty"`
	Color                      interface{} `json:"color,omitempty"`
	WomenLike                  string      `json:"women_like,omitempty"`
	Summary                    interface{} `json:"summary,omitempty"`
	ManufacturerInfo           string      `json:"manufacturer_info,omitempty"`
	BlakstienuIlgis            interface{} `json:"blakstienu_ilgis,omitempty"`
	BlakstienuPlotis           interface{} `json:"blakstienu_plotis,omitempty"`
	ColorswatchColor           string      `json:"colorswatch_color,omitempty"`
	GroupPrice                 []string   `json:"group_price,omitempty"`
}

type NewProduct struct {
	EntityID             string      `json:"entity_id"`
	Sku                  string      `json:"sku"`
	Name                 string      `json:"name"`
	URL                  string      `json:"url"`
	Price                string      `json:"price"`
	Qty                  int         `json:"qty"`
	IsInStock            string      `json:"is_in_stock"`
	Status               string      `json:"status"`
	CreatedAt            string      `json:"created_at"`
	UpdatedAt            string      `json:"updated_at"`
	Description          string      `json:"description"`
	ShortDescription     interface{} `json:"short_description"`
	Brand                bool        `json:"brand"`
	Supplier             bool        `json:"supplier"`
	Color                interface{} `json:"color"`
	Size                 interface{} `json:"size"`
	Manufacturer         bool        `json:"manufacturer"`
	CountryOfManufacture interface{} `json:"country_of_manufacture"`
	SpecialPrice         interface{} `json:"special_price"`
	GroupPrice           string      `json:"group_price"`
	MediaGallery         string      `json:"media_gallery"`
	Weight               interface{} `json:"weight"`
	MetaTitle            string      `json:"meta_title"`
	MetaKeywords         string      `json:"meta_keywords"`
	MetaDescription      string      `json:"meta_description"`
	TaxClass             string      `json:"tax_class"`
	OtherAttributes      struct {
		EntityTypeID               string        `json:"entity_type_id"`
		AttributeSetID             string        `json:"attribute_set_id"`
		TypeID                     string        `json:"type_id"`
		HasOptions                 string        `json:"has_options"`
		RequiredOptions            string        `json:"required_options"`
		Cost                       interface{}   `json:"cost"`
		Msrp                       interface{}   `json:"msrp"`
		Image                      string        `json:"image"`
		SmallImage                 string        `json:"small_image"`
		Thumbnail                  string        `json:"thumbnail"`
		URLKey                     string        `json:"url_key"`
		URLPath                    string        `json:"url_path"`
		CustomDesign               interface{}   `json:"custom_design"`
		PageLayout                 interface{}   `json:"page_layout"`
		OptionsContainer           string        `json:"options_container"`
		MsrpEnabled                string        `json:"msrp_enabled"`
		MsrpDisplayActualPriceType string        `json:"msrp_display_actual_price_type"`
		GiftMessageAvailable       interface{}   `json:"gift_message_available"`
		FoundInRivileAPI           string        `json:"found_in_rivile_api"`
		IsRecurring                string        `json:"is_recurring"`
		Visibility                 string        `json:"visibility"`
		EnableGooglecheckout       string        `json:"enable_googlecheckout"`
		TaxClassID                 string        `json:"tax_class_id"`
		IsImported                 string        `json:"is_imported"`
		Featured                   string        `json:"featured"`
		MageFeaturedProduct        string        `json:"mage_featured_product"`
		Date                       string        `json:"date"`
		ProductWeight              interface{}   `json:"product_weight"`
		MetaKeyword                string        `json:"meta_keyword"`
		CustomLayoutUpdate         interface{}   `json:"custom_layout_update"`
		SpecialFromDate            interface{}   `json:"special_from_date"`
		SpecialToDate              interface{}   `json:"special_to_date"`
		NewsFromDate               interface{}   `json:"news_from_date"`
		NewsToDate                 interface{}   `json:"news_to_date"`
		CustomDesignFrom           interface{}   `json:"custom_design_from"`
		CustomDesignTo             interface{}   `json:"custom_design_to"`
		GroupPriceChanged          int           `json:"group_price_changed"`
		TierPrice                  []interface{} `json:"tier_price"`
		TierPriceChanged           int           `json:"tier_price_changed"`
		StockItem                  struct{}      `json:"stock_item"`
		IsSalable                  interface{}   `json:"is_salable"`
		RequestPath                bool          `json:"request_path"`
	} `json:"other_attributes,omitempty"`
}

type Category struct {
	EntityID                   string      `json:"entity_id"`
	ParentID                   string      `json:"parent_id"`
	CreatedAt                  string      `json:"created_at"`
	UpdatedAt                  string      `json:"updated_at"`
	Path                       string      `json:"path"`
	Position                   string      `json:"position"`
	Level                      string      `json:"level"`
	ChildrenCount              string      `json:"children_count"`
	StoreID                    string      `json:"store_id"`
	Addtocart                  interface{} `json:"addtocart"`
	AllChildren                interface{} `json:"all_children"`
	AvailableSortBy            interface{} `json:"available_sort_by"`
	CategoryDescriptionTpl     interface{} `json:"category_description_tpl"`
	CategoryMetaDescriptionTpl interface{} `json:"category_meta_description_tpl"`
	CategoryMetaKeywordsTpl    interface{} `json:"category_meta_keywords_tpl"`
	CategoryMetaTitleTpl       interface{} `json:"category_meta_title_tpl"`
	CategoryTitleTpl           interface{} `json:"category_title_tpl"`
	Children                   interface{} `json:"children"`
	Compareproducts            interface{} `json:"compareproducts"`
	CustomApplyToProducts      interface{} `json:"custom_apply_to_products"`
	CustomDesign               interface{} `json:"custom_design"`
	CustomDesignFrom           interface{} `json:"custom_design_from"`
	CustomDesignTo             interface{} `json:"custom_design_to"`
	CustomLayoutUpdate         interface{} `json:"custom_layout_update"`
	CustomUseParentSettings    interface{} `json:"custom_use_parent_settings"`
	DefaultSortBy              interface{} `json:"default_sort_by"`
	Description                interface{} `json:"description"`
	DisplayMode                interface{} `json:"display_mode"`
	FilterDescriptionTpl       interface{} `json:"filter_description_tpl"`
	FilterMetaDescriptionTpl   interface{} `json:"filter_meta_description_tpl"`
	FilterMetaKeywordsTpl      interface{} `json:"filter_meta_keywords_tpl"`
	FilterMetaTitleTpl         interface{} `json:"filter_meta_title_tpl"`
	FilterPriceRange           interface{} `json:"filter_price_range"`
	FilterTitleTpl             interface{} `json:"filter_title_tpl"`
	Image                      interface{} `json:"image"`
	IncludeInMenu              string      `json:"include_in_menu"`
	IsActive                   interface{} `json:"is_active"`
	IsAnchor                   interface{} `json:"is_anchor"`
	LandingPage                interface{} `json:"landing_page"`
	Mailtofriend               interface{} `json:"mailtofriend"`
	MetaDescription            interface{} `json:"meta_description"`
	MetaKeywords               interface{} `json:"meta_keywords"`
	MetaTitle                  interface{} `json:"meta_title"`
	Name                       string      `json:"name"`
	PageLayout                 interface{} `json:"page_layout"`
	PathInStore                interface{} `json:"path_in_store"`
	Productname                interface{} `json:"productname"`
	Productprice               interface{} `json:"productprice"`
	ProductDescriptionTpl      interface{} `json:"product_description_tpl"`
	ProductMetaDescriptionTpl  interface{} `json:"product_meta_description_tpl"`
	ProductMetaKeywordsTpl     interface{} `json:"product_meta_keywords_tpl"`
	ProductMetaTitleTpl        interface{} `json:"product_meta_title_tpl"`
	ProductTitleTpl            interface{} `json:"product_title_tpl"`
	Quickview                  interface{} `json:"quickview"`
	Sidebarhider               interface{} `json:"sidebarhider"`
	Thumbnail                  interface{} `json:"thumbnail"`
	URLKey                     interface{} `json:"url_key"`
	URLPath                    interface{} `json:"url_path"`
	Wishlist                   interface{} `json:"wishlist"`
	Youtubecode                interface{} `json:"youtubecode"`
}


type ProductB struct {
	EntityID             string          `json:"entity_id"`
	Sku                  string          `json:"sku"`
	Name                 string          `json:"name"`
	URL                  string          `json:"url"`
	Price                string          `json:"price"`
	Qty                  int64           `json:"qty"`
	IsInStock            string          `json:"is_in_stock"`
	Status               string          `json:"status"`
	CreatedAt            string          `json:"created_at"`
	UpdatedAt            string          `json:"updated_at"`
	Description          string          `json:"description"`
	ShortDescription     interface{}     `json:"short_description"`
	Brand                *string         `json:"brand"`
	Supplier             *string         `json:"supplier"`
	Color                interface{}     `json:"color"`
	Size                 interface{}     `json:"size"`
	Manufacturer         *string         `json:"manufacturer"`
	CountryOfManufacture interface{}     `json:"country_of_manufacture"`
	SpecialPrice         *string         `json:"special_price"`
	GroupPrice           string          `json:"group_price"`
	MediaGallery         string          `json:"media_gallery"`
	Weight               interface{}     `json:"weight"`
	MetaTitle            *string         `json:"meta_title"`
	MetaKeywords         *string         `json:"meta_keywords"`
	MetaDescription      *string         `json:"meta_description"`
	TaxClass             string          `json:"tax_class"`
	OtherAttributes      OtherAttributes `json:"other_attributes"`
}

type OtherAttributes struct {
	EntityTypeID               string           `json:"entity_type_id"`
	AttributeSetID             string           `json:"attribute_set_id"`
	TypeID                     string           `json:"type_id"`
	HasOptions                 string           `json:"has_options"`
	RequiredOptions            string           `json:"required_options"`
	Cost                       interface{}      `json:"cost"`
	Msrp                       interface{}      `json:"msrp"`
	Image                      string           `json:"image"`
	SmallImage                 string           `json:"small_image"`
	Thumbnail                  string           `json:"thumbnail"`
	URLKey                     string           `json:"url_key"`
	URLPath                    string           `json:"url_path"`
	CustomDesign               interface{}      `json:"custom_design"`
	PageLayout                 interface{}      `json:"page_layout"`
	OptionsContainer           string `json:"options_container"`
	MsrpEnabled                string           `json:"msrp_enabled"`
	MsrpDisplayActualPriceType string           `json:"msrp_display_actual_price_type"`
	GiftMessageAvailable       interface{}      `json:"gift_message_available"`
	FoundInRivileAPI           *string          `json:"found_in_rivile_api"`
	IsRecurring                *string          `json:"is_recurring,omitempty"`
	Visibility                 string           `json:"visibility"`
	EnableGooglecheckout       string           `json:"enable_googlecheckout"`
	TaxClassID                 string           `json:"tax_class_id"`
	IsImported                 string           `json:"is_imported"`
	Featured                   string           `json:"featured"`
	MageFeaturedProduct        string           `json:"mage_featured_product"`
	Date                       *string          `json:"date"`
	ProductWeight              interface{}      `json:"product_weight"`
	MetaKeyword                *string          `json:"meta_keyword"`
	CustomLayoutUpdate         interface{}      `json:"custom_layout_update"`
	SpecialFromDate            *string          `json:"special_from_date"`
	SpecialToDate              interface{}      `json:"special_to_date"`
	NewsFromDate               *string          `json:"news_from_date"`
	NewsToDate                 *string          `json:"news_to_date"`
	CustomDesignFrom           interface{}      `json:"custom_design_from"`
	CustomDesignTo             interface{}      `json:"custom_design_to"`
	GroupPriceChanged          int64            `json:"group_price_changed"`
	TierPrice                  []interface{}    `json:"tier_price"`
	TierPriceChanged           int64            `json:"tier_price_changed"`
	StockItem                  struct{}         `json:"stock_item"`
	IsSalable                  interface{}      `json:"is_salable"`
	RequestPath                bool             `json:"request_path"`
	ImageLabel                 *string          `json:"image_label"`
	SmallImageLabel            *string          `json:"small_image_label"`
	ThumbnailLabel             *string          `json:"thumbnail_label"`
	SmdCustomStockStatus       interface{}      `json:"smd_custom_stock_status"`
	UseSmdColorswatch          *string          `json:"use_smd_colorswatch,omitempty"`
	SmdColorswatchProductList  *string          `json:"smd_colorswatch_product_list,omitempty"`
	WomenLike                  *string          `json:"women_like,omitempty"`
	Wights                     interface{}      `json:"wights"`
	Provider                   *string          `json:"provider,omitempty"`
	SoldToProfessionals        *string          `json:"sold_to_professionals,omitempty"`
	BlakstienuPlotis           interface{}      `json:"blakstienu_plotis"`
	BlakstienuIlgis            interface{}      `json:"blakstienu_ilgis"`
	Summary                    *string          `json:"summary"`
	ManufacturerInfo           *string          `json:"manufacturer_info"`
}

